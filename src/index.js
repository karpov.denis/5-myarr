class MyArray {
  constructor(...args) {
    Object.defineProperty(this, 'length', {
      enumerable: false,
      writable: true,
      configurable: false
    });

    const varLength = {
      '0': () => (this.length = 0),

      '1': () => {
        const primitiveFlag =
          typeof args[0] === 'string' ||
          args[0] === undefined ||
          args[0] === null;

        const errorFlag =
          typeof args[0] !== 'string' &&
          (!Number.isInteger(args[0]) || args[0] === Infinity || args[0] < 0);

        if (primitiveFlag) {
          this.length = 1;
          this[0] = args[0];
        }

        if (errorFlag) {
          throw new Error('Invalid array length');
        }

        if (!errorFlag && !primitiveFlag) {
          this.length = args[0];
        }
      },

      default: () => {
        this.length = args.length;

        for (let i = 0; i < this.length; i++) {
          this[i] = args[i];
        }
      }
    };

    (varLength[args.length] || varLength['default'])();
  }

  [Symbol.iterator]() {
    let index = 0;
    const that = this;

    return {
      next() {
        if (index < that.length) {
          index += 1;
          return {
            value: that[index - 1],
            done: false
          };
        } else {
          return {
            done: true
          };
        }
      }
    };
  }

  push(...args) {
    for (let i = 0; i < args.length; i++) {
      this[i + this.length] = args[i];
    }
    this.length = args.length + this.length;
    return this.length;
  }

  pop() {
    if (this.length === 0) {
      return undefined;
    }

    const lastEl = this[this.length - 1];
    this.length = this.length - 1;
    return lastEl;
  }

  unshift(item, ...args) {
    if (item === undefined) {
      return this.length;
    }

    const fullArr = new MyArray(item, ...args);
    this.length += fullArr.length;

    for (let i = this.length - 1; i >= fullArr.length; i--) {
      this[i] = this[i - fullArr.length];
    }

    for (let j = 0; j < fullArr.length; j++) {
      this[j] = fullArr[j];
    }
    return this.length;
  }

  shift() {
    if (this.length === 0) {
      return undefined;
    }

    const firstEl = this[0];

    for (let i = 1; i < this.length; i++) {
      this[i - 1] = this[i];
    }
    delete this[this.length - 1];
    this.length = this.length - 1;
    return firstEl;
  }

  static from(arrLike, ...args) {
    if (arrLike.length === undefined) {
      return new MyArray();
    }

    // if (
    //   arguments[1] !== undefined &&
    //   (typeof mapFn !== 'function' ||
    //     Object.prototype.toString.call(mapFn) !== '[object Function]')
    // ) {
    //   throw new TypeError(
    //     'myArr.from: when provided, the second argument must be a function'
    //   );
    // }
    // mapFn, thisArg

    const result = new MyArray();
    result.length = arrLike.length;

    for (let i = 0; i < arrLike.length; i++) {
      args[0]
        ? (result[i] = args[0].call(args[1], arrLike[i]))
        : (result[i] = arrLike[i]);
    }

    return result;
  }

  forEach(callback, ...args) {
    // if (
    //   typeof callback !== 'function' ||
    //   Object.prototype.toString.call(mapFn) !== '[object Function]'
    // ) {
    //   throw new TypeError(`${callback} is not a function`);
    // }
    const currentLen = this.length;

    for (let i = 0; i < currentLen; i++) {
      if (i in this) {
        callback.call(args[0], this[i], i, this);
      }
    }
  }

  map(callback, ...args) {
    // if (
    //   typeof callback !== 'function' ||
    //   Object.prototype.toString.call(mapFn) !== '[object Function]'
    // ) {
    //   throw new TypeError(`${callback} is not a function`);
    // }

    const result = new MyArray();
    result.length = this.length;

    for (let i = 0; i < result.length; i++) {
      if (i in this) {
        result[i] = callback.call(args[0], this[i], i, this);
      }
    }
    return result;
  }

  reduce(callback, ...args) {
    // if (
    //   typeof callback !== 'function' ||
    //   Object.prototype.toString.call(callback) !== '[object Function]'
    // ) {
    //   throw new TypeError(`${callback} is not a function`);
    // }
    if (args.length > 0 && this.length === 0) {
      return args[0];
    }

    let i = 0;

    let result = args[0];

    let k = 0;

    while (k < this.length && !(k in this)) {
      k += 1;
    }

    if (k >= this.length) {
      throw new TypeError('Reduce of empty array with no initial value');
    }

    args[0] ? (i = 0) : ((i = 1), (result = this[0]));

    for (i; i < this.length; i++) {
      result = callback(result, this[i], i, this);
    }
    return result;
  }

  filter(callback, ...args) {
    // if (
    //   typeof callback !== 'function' ||
    //   Object.prototype.toString.call(callback) !== '[object Function]'
    // ) {
    //   throw new TypeError(`${callback} is not a function`);
    // }

    const result = new MyArray();
    const сurrentLen = this.length;
    const newThisArg = args.length > 0 ? args[0] : undefined;

    for (let i = 0; i < сurrentLen; i++) {
      if (i in this) {
        if (callback.call(newThisArg, this[i], i, this)) {
          result.push(this[i]);
        }
      }
    }
    return result;
  }

  sort(compareFunction) {
    let func = function(a, b) {
      const x = String(a);
      const y = String(b);

      if (x < y) {
        return -1;
      }

      if (x > y) {
        return 1;
      }
      return 0;
    };

    compareFunction ? (func = compareFunction) : func;

    let undefinedFlag = false;
    let temp = 0;

    for (let i = 0, endI = this.length - 1; i < endI; i++) {
      undefinedFlag = false;

      for (let j = 0, endJ = endI - i; j < endJ; j++) {
        if (this[j + 1] === undefined) {
          undefinedFlag = true;
        } else if (this[j] === undefined || func(this[j], this[j + 1]) > 0) {
          temp = this[j + 1];
          this[j + 1] = this[j];
          this[j] = temp;
          undefinedFlag = true;
        }
      }

      if (!undefinedFlag) {
        break;
      }
    }
    return this;
  }

  toString() {
    let str = '';

    for (let i = 0; i < this.length; i++) {
      let elem = this[i];
      this[i] === undefined || this[i] === null
        ? (elem = '')
        : (elem = this[i]);
      i === this.length - 1 ? (str += elem) : (str = `${str + elem},`);
    }

    return str;
  }

  slice(start, end) {
    const result = new MyArray();
    let constStart = start || 0;

    if (typeof constStart !== 'number' && typeof constStart !== 'string') {
      constStart = 0;
    }

    let constEnd = end || this.length;
    constStart < 0 ? (constStart = this.length + constStart) : undefined;
    constEnd < 0 ? (constEnd = this.length + constEnd) : undefined;

    for (let i = constStart; i < constEnd; i++) {
      result[result.length] = this[i];
      result.length += 1;
    }
    return result;
  }

  splice(start, deleteCount, ...args) {
    const result = new MyArray();
    const startLoop = start < 0 ? this.length + start : start;
    const len = this.length;

    if (deleteCount === undefined) {
      for (let i = startLoop; i < len; i++) {
        result[result.length] = this[i];
        result.length += 1;
        delete this[i];
        this.length -= 1;
      }
    }

    if (deleteCount > 0) {
      let endLoop = this.length;

      if (startLoop + deleteCount < this.length) {
        endLoop = startLoop + deleteCount;
      }

      for (let i = startLoop; i < this.length; i++) {
        if (i < endLoop) {
          result[result.length] = this[i];
          result.length += 1;
        }
        this[i] = this[i + deleteCount];
      }

      for (let i = startLoop; i < endLoop; i++) {
        delete this[i];
        this.length -= 1;
      }
    }

    if (args.length > 0) {
      const oldObj = Object.assign(new MyArray(), this);
      oldObj.length = this.length;

      for (let i = startLoop; i < oldObj.length + args.length; i++) {
        if (i - startLoop < args.length) {
          this[i] = args[i - startLoop];
        } else {
          this[i] = oldObj[i - args.length];
        }
      }
      this.length += args.length;
    }

    return result;
  }
}
export default MyArray;
