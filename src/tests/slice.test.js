import MyArray from '../index';

describe('tests for slice method', () => {
  test('instance has not Own Property slice', () => {
    const arr = new MyArray();

    expect(Object.prototype.hasOwnProperty.call(arr, 'slice')).toBeFalsy();
  });

  test('instance has method slice', () => {
    const arr = new MyArray();
    expect(arr.slice).toBeInstanceOf(Function);
  });

  test('should return new array containing extracted items', () => {
    const arr = new MyArray(1, 2, 4, 5);

    const test1 = arr.slice();
    const test2 = arr.slice(0, 3);
    const test3 = arr.slice(1, 2);
    const test4 = arr.slice(2, -1);
    const test5 = arr.slice(0, 1);

    const result1 = new MyArray(1, 2, 4, 5);
    const result2 = new MyArray(1, 2, 4);
    const result3 = new MyArray(1);
    result3[0] = 2;
    const result4 = new MyArray(1);
    result4[0] = 4;
    const result5 = new MyArray(1);
    result5[0] = 1;

    expect(test1).toEqual(result1);
    expect(test2).toEqual(result2);
    expect(test3).toEqual(result3);
    expect(test4).toEqual(result4);
    expect(test5).toEqual(result5);
  });

  test('should return a copy of array, if method called without arguments', () => {
    const arr = new MyArray(1, 2, 4, 5);
    expect(arr.slice()).toEqual(arr);
  });

  test('should not mutate the original array', () => {
    const arr = new MyArray(1, 2, 4, 5);
    arr.slice();
    expect(arr).toEqual(new MyArray(1, 2, 4, 5));
    expect(arr).toHaveLength(4);
  });

  test('result array should be instance of array class', () => {
    const arr = new MyArray(1, 2, 4, 5);
    const arr1 = arr.slice();

    // expect(arr1 instanceof MyArray).toBeTruthy();
    expect(arr1).toBeInstanceOf(MyArray);
  });

  test('if 2nd argument is not specified, slice selects all elements to the end arr.length', () => {
    const arr = new MyArray(1, 2, 4, 5);
    const result = arr.slice(1);
    const test = new MyArray(2, 4, 5);

    expect(result).toEqual(test);
  });

  test('can take 2 required argument', () => {
    const arr = new MyArray();
    expect(arr.slice).toHaveLength(2);
  });

  test('1st argument should be a start index for trimmed', () => {
    const arr = new MyArray(1, 2, 4, 5);
    const test1 = new MyArray(4, 5);
    expect(arr.slice(2)).toEqual(test1);
  });

  test('2nd argument should be amount of returned elements', () => {
    const arr = new MyArray(1, 2, 4, 5);
    const test1 = new MyArray(1, 2);
    expect(arr.slice(0, 2)).toEqual(test1);
  });

  test('should work with negative numbers as arguments', () => {
    const arr = new MyArray(1, 2, 3, 4, 5);
    const test1 = new MyArray(2, 3);
    expect(arr.slice(-4, -2)).toEqual(test1);
  });

  test('should return empty array if 1st argument is equal to array length', () => {
    const arr = new MyArray(1, 2, 3, 4, 5);
    const test1 = new MyArray();
    expect(arr.slice(5)).toEqual(test1);
  });

  test('should work with numbers as strings as arguments', () => {
    const arr = new MyArray(1, 2, 3, 4, 5);
    const test1 = new MyArray(1);
    test1[0] = 4;
    expect(arr.slice('3', '-1')).toEqual(test1);
  });

  test('should return empty array if both arguments is not a number', () => {
    const arr = new MyArray(1, 2, 3, 4, 5);
    const test1 = new MyArray();
    expect(arr.slice({}, 'string')).toEqual(test1);
  });

  test('should return copy of array if 1st argument is not a number', () => {
    const arr = new MyArray(1, 2, 3, 4, 5);
    expect(arr.slice({})).toEqual(arr);
  });

  test('should work with empty elements', () => {
    const arr = new MyArray(1, 2, 3, 4, 4, 4);
    delete arr[3];
    delete arr[4];
    expect(arr.slice(0, 4)).toHaveLength(4);
  });
});
